import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Contact } from '../../models/contact.model';

@Component({
  selector: 'uc-contact-chip',
  templateUrl: './contact-chip.component.html',
  styleUrls: ['./contact-chip.component.scss']
})
export class ContactChipComponent {

  @Input() index: number;
  @Input() contact: Contact;

  @Output() close: EventEmitter<number> = new EventEmitter();

  $close() {
    this.close.emit(this.index);
  }

  get hasImage(): boolean {
    return !!this.contact.image;
  }

  get image(): string {
    return this.contact.image;
  }

  get text(): string {
    if (!!this.contact.emailAddress && !!this.contact.lastName) {
      return this.contact.firstName + ' ' + this.contact.lastName;
    } else {
      return this.contact.emailAddress;
    }
  }
}
