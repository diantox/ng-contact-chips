import { Component, Input, Output, EventEmitter } from '@angular/core';
import { debounce } from 'lodash';

import { Contact } from '../../models/contact.model';

@Component({
  selector: 'uc-contact-input',
  templateUrl: './contact-input.component.html',
  styleUrls: ['./contact-input.component.scss']
})
export class ContactInputComponent {

  readonly emailAddressRegExp: RegExp = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

  @Input() placeholder: string;
  @Input() knownContacts: Contact[] = [];
  matchedContacts: Contact[] = [];

  @Output() selectedContact: EventEmitter<Contact> = new EventEmitter();

  value: string = '';
  d$keypress: Function;

  constructor() {
    this.d$keypress = debounce(this.$keypress, 500);
  }

  $keypress($event: KeyboardEvent) {
    const target = $event.target as HTMLInputElement;
    const value = target.value;
    this.value = value;

    if (!!this.value) {
      this.matchContacts();
    } else {
      this.matchedContacts = [];
    }

    if ($event.key === 'Enter') {
      this.selectContactFromInput();
    }
  }

  matchContacts() {
    const value_lc = this.value.toLowerCase();

    this.matchedContacts = this.knownContacts.filter(contact => {
      const firstName_lc = contact.firstName.toLowerCase();
      const lastName_lc = contact.lastName.toLowerCase();
      const emailAddress_lc = contact.emailAddress.toLowerCase();

      return firstName_lc.includes(value_lc) || lastName_lc.includes(value_lc) || emailAddress_lc.includes(value_lc);
    }).slice(0, 5);
  }

  selectContactFromInput() {
    if (this.emailAddressRegExp.test(this.value)) {
      if (this.matchedContacts.length === 1) {
        const value_lc = this.value.toLowerCase();
        const emailAddress_lc = this.matchedContacts[0].emailAddress.toLowerCase();

        if (value_lc === emailAddress_lc) {
          this.selectContact(this.matchedContacts[0]);
        }
      } else {
        const contact: Contact = {
          image: '',
          firstName: '',
          lastName: '',
          emailAddress: this.value
        };

        this.selectContact(contact);
      }
    }
  }

  selectContact(contact: Contact) {
    this.selectedContact.emit(contact);

    this.value = '';
    this.matchedContacts = [];
  }
}
