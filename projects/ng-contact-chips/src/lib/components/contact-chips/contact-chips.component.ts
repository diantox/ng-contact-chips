import { Component, Input, Output, EventEmitter } from '@angular/core';
import { includes } from 'lodash';

import { Contact } from '../../models/contact.model';

@Component({
  selector: 'uc-contact-chips',
  templateUrl: './contact-chips.component.html',
  styleUrls: ['./contact-chips.component.scss']
})
export class ContactChipsComponent {

  @Input() placeholder: string = 'Contact';
  @Input() knownContacts: Contact[] = [];
  @Input() contacts: Contact[] = [];

  @Output() contactsChange: EventEmitter<Contact[]> = new EventEmitter();

  $close(index: number) {
    this.contacts.splice(index, 1);
    this.contactsChange.emit(this.contacts);
  }

  $selectedContact(contact: Contact) {
    if (!includes(this.contacts, contact)) {
      this.contacts.push(contact);
      this.contactsChange.emit(this.contacts);
    }
  }
}
