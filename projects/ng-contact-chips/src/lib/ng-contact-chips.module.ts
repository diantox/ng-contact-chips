import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContactChipsComponent } from './components/contact-chips/contact-chips.component';
import { ContactChipComponent } from './components/contact-chip/contact-chip.component';
import { ContactInputComponent } from './components/contact-input/contact-input.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [ContactChipsComponent, ContactChipComponent, ContactInputComponent],
  exports: [ContactChipsComponent]
})
export class NgContactChipsModule { }
