import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgContactChipsModule } from 'projects/ng-contact-chips/src/public_api';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    NgContactChipsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
